import { Component, h, State, Prop, Listen} from '@stencil/core';
import {AssignmentsHttpService} from "../../http_services/assignments.http_service";
import _ from "underscore";
import { modalController} from "@ionic/core";

@Component({
  tag: 'app-assignments',
})
export class AppAssignments {

  @Prop() courseId: any;
  @State() assignments: any;
  @State() modal:any;

  async componentWillLoad(){
    this.assignments = await new AssignmentsHttpService().where({course_id: this.courseId});
  }

  private router = document.querySelector('ion-router');

  clickedBackButton(){
    this.router.push(`/courses`, "back")
  }


  @Listen('ionModalWillDismiss', {target: 'body'})
  async update(event) {
    event.detail.data.course_id = this.courseId;
    if (event.detail.role === 'app-add-assignment-modal'){
      const response = await new AssignmentsHttpService().create(event.detail.data);
      this.assignments = [...this.assignments, response];
    }
    else if (event.detail.role === 'delete-assignment') {
      await new AssignmentsHttpService().delete(event.detail.data.id);
      this.assignments = await new AssignmentsHttpService().where({course_id: this.courseId});
    }
    else if (event.detail.role === 'update-assignment') {
      await new AssignmentsHttpService().update(event.detail.data);
      const oldAssignment = await _.findWhere(this.assignments, {id: event.detail.data.id});
      const index = _.findIndex(this.assignments, oldAssignment);
      let newCopy = [...this.assignments];
      newCopy[index] = event.detail.data;
      this.assignments = [...newCopy];
    }
  }

  async openAddAssignmentModal() {
    this.modal = await modalController.create({
      component: 'app-add-assignment-modal',
      componentProps: {
        courseId: this.courseId
      }
    });
    await this.modal.present();
  }

  listDatesAndAssignments(){
    let sortedByDateAssignments = _.sortBy(this.assignments, 'due_date');
    let groupedByDate = _.groupBy(sortedByDateAssignments, 'due_date');
    let groupedAssignmentsArray = _.toArray(groupedByDate);
    // let groupedAssignments = _.toArray( _.groupBy ( _.sortBy(this.assignments, 'due_date'), 'due_date'));
    return[
      groupedAssignmentsArray.map((assignmentGroup) => {
        let assignmentGroupDueDate = assignmentGroup[0]['due_date'];
        return [
          <app-list-dates assignmentGroup = {assignmentGroup} assignmentGroupDueDate = {assignmentGroupDueDate}/>
        ]
      })
    ]
  }

  renderAssignmentImage(){
    if(this.assignments.length !== 0){
      return [
        <ion-col size={"4"}>
         <img src={"../assets/images/BTN_AddAssignment.svg"} onClick={() => {this.openAddAssignmentModal()}}/>
        </ion-col>
      ]
    } else {
      return [
        <ion-col size="10">
          <img src={"../assets/images/Illo-robot.svg"} onClick={() => {this.openAddAssignmentModal()}}/>
        </ion-col>,
        <p>Our robots are working hard uploading other assignments. Would you like to manually upload them?</p>,
        <ion-text
          class={"ion-text-uppercase"}
          color="tertiary"
          onClick={() => this.openAddAssignmentModal()}
        >
          Input Manual Assignments
        </ion-text>
      ]
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot={"start"}>
            <ion-button color={"tertiary"}  onClick={() => this.clickedBackButton()}>
              <ion-icon name="arrow-back"/>
            </ion-button>
          </ion-buttons>
          <ion-title>Assignments</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={"light"} text-center={true}>
        {this.listDatesAndAssignments()}
        <ion-grid>
          <ion-row class={"ion-justify-content-center"}>
              {this.renderAssignmentImage()}
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
