import { Component, h, Prop} from '@stencil/core';
import {format, isThisWeek, isToday, isTomorrow, getDay, isPast} from "date-fns";

@Component({
  tag: 'app-list-dates',
})
export class AppListDates {

  @Prop() assignmentGroupDueDate: any;
  @Prop() assignmentGroup: any;

  listAllAssignmentsForDate(){
    return[
    this.assignmentGroup.map( (assignment) => {
        return[
          <app-list-assignments assignment={assignment}/>
        ] } )
    ]
  }

  formatAssignmentGroupDueDate(){
    let today = new Date();
    if ( isToday(this.assignmentGroupDueDate)){
      return ['Today']
    } else if ( isTomorrow( this.assignmentGroupDueDate)){
      return ['Tomorrow']
    } else if (isThisWeek((this.assignmentGroupDueDate), {weekStartsOn:getDay(today)} )){
      return[ format( this.assignmentGroupDueDate, 'dddd')]
    } else if ( isPast( this.assignmentGroupDueDate)){
      return[ `Past due: ${format( this.assignmentGroupDueDate, 'MMMM DD, YYYY')}`]
    } else {
        return[ format( this.assignmentGroupDueDate, 'MMMM DD, YYYY') ]
    }
  }

  render() {
    return [
      <ion-item lines={'none'} color={"light"}>
        {this.formatAssignmentGroupDueDate()}
      </ion-item>,

      <ion-card style={{marginTop: "0", marginBottom: "0"}}>
        {this.listAllAssignmentsForDate()}
      </ion-card>
    ];
  }
}
