import { Component, h, Element} from '@stencil/core';

@Component({
  tag: 'app-add-assignment-modal',
})
export class AppAddAssignmentModal {

  name:HTMLIonInputElement;
  due_date:HTMLIonDatetimeElement;
  description:HTMLIonTextareaElement;
  possible_points:HTMLIonInputElement;
  nameError:HTMLIonNoteElement;
  dateError:HTMLIonNoteElement;
  descriptionError:HTMLIonNoteElement;
  pointsError:HTMLIonNoteElement;

  @Element() el: Element;

  dismiss(){
    (this.el.closest('ion-modal') as any).dismiss();
  }

  addAssignment(){
    (this.el.closest('ion-modal') as any).dismiss({name: this.name.value, due_date: this.due_date.value,
      description: this.description.value, possible_points: this.possible_points.value}, 'app-add-assignment-modal');
  }

  validateForm() {
    let inputs = [this.name, this.due_date, this.description, this.possible_points];
    let notes = [this.nameError, this.dateError, this.descriptionError, this.pointsError];
    let err = 0;
    for (let i=0;i<4;i++){
      if (inputs[i].value === '' || inputs[i].value === undefined) {
        inputs[i].classList.add('failLine');
        notes[i].classList.remove('hiddenMessage');
        err +=1
      }
      else if (inputs[i].classList.contains('failLine')) {
        inputs[i].classList.remove('failLine');
        notes[i].classList.add('hiddenMessage');
      }
    }
    if (err === 0) {
      this.addAssignment()
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>
            Add Assignments
            </ion-title>
          <ion-buttons slot={'end'}>
            <ion-button color={"tertiary"} onClick={()=>{this.dismiss()}}>
              <ion-label>
                Cancel
              </ion-label>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'}>
        <ion-card>
          <ion-list lines={'none'}>
            <ion-item>
              <ion-label position={"floating"}>
                Assignment name *
              </ion-label>
              <ion-input
                placeholder={'Exam 01'}
                ref={el => this.name = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.nameError = el as HTMLIonNoteElement}>
                Name is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Due Date *
              </ion-label>
              <ion-datetime
                min="2018"
                max="2030"
                placeholder={'mm/dd/yyyy'}
                ref={el => this.due_date = el as HTMLIonDatetimeElement}
                class={'passLine'}
              >
              </ion-datetime>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.dateError = el as HTMLIonNoteElement}>
                Due date is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Description *
              </ion-label>
              <ion-textarea
                rows={5}
                ref={el => this.description = el as HTMLIonTextareaElement}
                placeholder={'Write a twelve page essay\n' + '\n' +
                'Lorem ipsum dolor sit amet, ut vis meis aperiri sadipscing, tempor oblique delectus mel ne, ' +
                'ut vide sale consul pro. Rebum fastidii id pri. Per eu illud pertinax. Eu integre saperet qui.'}
                class={'passLine'}
              >
              </ion-textarea>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.descriptionError = el as HTMLIonNoteElement}>
                Assignment description is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Possible Points *
              </ion-label>
              <ion-input
                placeholder={'50'}
                ref={el => this.possible_points = el as HTMLIonInputElement}
                type="number"
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.pointsError = el as HTMLIonNoteElement}>
                Possible points is required
              </ion-note>
            </ion-item>
            <ion-button strong={true} color={"secondary"} expand={"full"} onClick={()=>{this.validateForm()}}>
              <ion-label>
                CREATE ASSIGNMENT
              </ion-label>
            </ion-button>
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}

