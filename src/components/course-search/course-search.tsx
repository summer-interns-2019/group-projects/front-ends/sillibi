import {Component, h, Element, Prop, State} from '@stencil/core';
import {CourseHttpService} from "../../http_services/course.http_service";
import {CoursesUsersHttpService} from "../../http_services/courses_users.http_service";
import _ from "underscore"
import {ProfileHttpService} from "../../http_services/profile.http_service";

@Component({
  tag: 'course-search',
})
export class CourseSearch {
  searchTerm:HTMLIonSearchbarElement;

  @Element() el: Element;
  @Prop() profile: any;
  @State() courseItems: any;
  @Prop() userId: any;
  @Prop() enrolledCourseIds: any;
  @Prop() enrolledCourses: any;
  @State() sillibiVerifiedCourses: any;
  @State() notEnrolledVerifiedCourses: any;


  async componentWillLoad() {
    this.profile = await new ProfileHttpService().where({user_id:this.userId});
    this.enrolledCourseIds = await new CoursesUsersHttpService().where({user_id: this.userId});
    await this.enrolledCourseIds.map((course) =>{
      let index = _.indexOf(this.enrolledCourseIds, course);
      this.enrolledCourseIds[index] = course.course_id;
    });
    this.sillibiVerifiedCourses = await new CourseHttpService().where({institution_id:this.profile.institution_id, verified:true});
    this.notEnrolledVerifiedCourses = await this.sillibiVerifiedCourses.filter(course =>
      !(_.contains(this.enrolledCourseIds, course.id,0))
    );
    this.courseItems = this.notEnrolledVerifiedCourses;
  }

  renderCourses() {
    if(this.notEnrolledVerifiedCourses.length === 0){
      return [
        <ion-grid text-center={true}>
          <ion-row>
            <ion-col>
              <img src={'../assets/icon/Illo-robot.svg'} alt={'no verified courses robot'}/>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              <ion-label>
                There are no verified courses at your institution yet. Please create a course and upload a syllabus.
              </ion-label>
            </ion-col>
          </ion-row>
        </ion-grid>
      ]
    } else{
      return this.courseItems.map((course) => {
        return[
          <ion-item
            lines={'none'}
            style={{borderBottom:`1px solid ${course.color}`}}
            onClick={_ => this.pushNewLink(`/search-course-info/${course.id}/${this.userId}`)}
          >
            <ion-label>
              {course.name}
            </ion-label>
            <ion-label>
              <ion-note>
                {course.number} - {course.section}
              </ion-note>
            </ion-label>
          </ion-item>
        ]
      })
    }
  }

  private router: HTMLIonRouterElement = document.querySelector('ion-router');
  pushNewLink(strLoc) {
    this.router.push(strLoc, 'forward');
  }

  handleInput() {
    this.courseItems = Array.from(this.notEnrolledVerifiedCourses);
    let val : string 	= this.searchTerm.value;
    if (val.trim() !== '') {
      this.courseItems = this.courseItems.filter((course) => {
        return course.name.toLowerCase().indexOf(val.toLowerCase()) > -1 || course.number.toLowerCase().indexOf(val.toLowerCase()) > -1;
      })
    }
  }


  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot={'start'}>
            <ion-button href={`/courses`}>
              <ion-icon color={'tertiary'} name={'arrow-back'}/>
            </ion-button>
          </ion-buttons>
          <ion-title>
            Course Search
          </ion-title>
        </ion-toolbar>
        <ion-toolbar>
          <ion-searchbar ref={el => this.searchTerm = el as HTMLIonSearchbarElement} placeholder={"Search For Verified Courses"} onIonChange={()=>{this.handleInput()}}/>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'}>
        <ion-card>
          <ion-list>
            {this.renderCourses()}
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}



