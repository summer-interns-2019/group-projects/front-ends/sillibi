import {Component, h, State, Prop, Listen} from '@stencil/core';
import {AssignmentsHttpService} from "../../http_services/assignments.http_service";
import {format} from "date-fns";
import {modalController} from "@ionic/core";

@Component({
  tag: 'app-view-assignment',
})
export class AppViewAssignment {

  @State() assignment: any;
  @Prop() courseId: any;
  @State() formattedDueDate: any;
  @Prop() assignmentId: any;
  @State() modal:any;

  async componentWillLoad() {
    this.assignment = await new AssignmentsHttpService().find(this.assignmentId);
    this.formattedDueDate = format(this.assignment.due_date, 'MMMM DD, YYYY');
  }

  @Listen('ionModalWillDismiss', {target: 'body'})
  async update(event) {
    if (event.detail.role === 'update-assignment') {
      let response = await new AssignmentsHttpService().update(event.detail.data);
      this.assignment = {...response};
      this.formattedDueDate = await format(event.detail.data.due_date, 'MMMM DD, YYYY');
    }
  }

  async openEditAssignmentModal() {
    this.modal = await modalController.create({
      component: 'app-edit-assignment-modal',
      componentProps: {
        assignment: this.assignment,
        courseId: this.assignment.course_id,
        assignmentId: this.assignment.id
      }
    });
    await this.modal.present();
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot={"start"}>
            <ion-back-button color={"tertiary"} text={""}/>
          </ion-buttons>
          <ion-title>Assignment</ion-title>
          <ion-buttons slot={"end"}>
            <ion-button color={"tertiary"} onClick={()=>{this.openEditAssignmentModal()}}>Edit</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={"light"}>
      <ion-card>
        <ion-item lines="none">
          <ion-label text-wrap>
            <ion-note>Assignment Name</ion-note>
            <h2>{this.assignment.name}</h2>
          </ion-label>
        </ion-item>
        <ion-item lines="none">
          <ion-label text-wrap>
            <ion-note>Due Date</ion-note>
            <h2>{this.formattedDueDate}</h2>
          </ion-label>
        </ion-item>
        <ion-item lines="none">
          <ion-label text-wrap>
            <ion-note>Description</ion-note>
            <h2>{this.assignment.description}</h2>
          </ion-label>
        </ion-item>
        <ion-item lines="full">
          <ion-label text-wrap>
            <ion-note>Possible Points</ion-note>
            <h2>{this.assignment.possible_points}</h2>
          </ion-label>
        </ion-item>
      </ion-card>
      </ion-content>
    ];
  }
}
