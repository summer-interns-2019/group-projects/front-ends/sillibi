import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-landing',
  styleUrl: 'app-landing.css'
})
export class AppLanding {


  componentWillLoad(){
    if(localStorage.token !== undefined){
      location.hash = '/courses'
    }
  }


  render() {
    return [
      <ion-content color="primary" text-center={true}>
        <ion-grid>
          <ion-row style={{textAlign: 'center', height:'50vh'}}>
            <ion-col>
              <img src="../../assets/icon/Sillibi-Logo.svg"/>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
              <ion-button expand="full" color="secondary" href="/login">
                LOGIN
              </ion-button>
            </ion-col>
          </ion-row>
          <ion-row style={{textAlign: 'center', height:'40vh'}}>
            <ion-col>
              <ion-label>
                <p>
                  Don't have an account?
                </p>
              </ion-label>
              <ion-button fill="clear" href="/register">
                <ion-text color="tertiary">
                  CREATE AN ACCOUNT
                </ion-text>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
