import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {
  render() {
    return (
      <ion-app>
        <ion-router useHash={true}>
          <ion-route url="/assignments/:courseId" component="app-assignments" />
          <ion-route url="/assignments-all" component="app-assignments-all-courses" />
          <ion-route url="/view-assignment/:assignmentId" component="app-view-assignment" />
          <ion-route url="/profile/:name" component="app-profile" />
          <ion-route url= "/" component="app-landing" />
          <ion-route url="/profile/:name" component="app-profile" />
          <ion-route url="/syllabuses/:courseId" component="course-syllabus"/>
          <ion-route url="/login" component="app-login-page" />
          <ion-route url="/register" component="app-register" />
          <ion-route url="/courses" component="my-courses" />
          <ion-route url="/course-info/:courseId" component="course-info" />
          <ion-route url="/course-search/:userId" component="course-search" />
          <ion-route url="/search-course-info/:courseId/:userId" component="course-info-from-search" />
          <ion-route url="/profile" component="app-profile-page" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
