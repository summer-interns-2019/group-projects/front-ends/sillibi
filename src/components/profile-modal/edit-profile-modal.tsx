import {Component, Element, h, Prop, State, Listen} from '@stencil/core';

@Component({
  tag: 'edit-profile-modal',
  styleUrl: 'edit-profile-modal.css'
})
export class EditProfileModal {
  first_name:HTMLIonInputElement;
  last_name:HTMLIonInputElement;
  email:any;
  phone:HTMLIonInputElement;
  @State() picture: string;
  phoneItem:HTMLIonItemElement;
  smsToggle:HTMLIonToggleElement;
  emailToggle:HTMLIonToggleElement;
  firstNameError:HTMLIonNoteElement;
  lastNameError:HTMLIonNoteElement;
  emailError:HTMLIonNoteElement;
  smsError:HTMLIonNoteElement;
  @Prop() profile: any;


  @Element() el: Element;
  avatarBase = "https://ui-avatars.com/api/?rounded=true&background=32134e&color=fff&bold=true&size=116&name=";
  oldPicture: string;

  componentWillRender() {
    if(this.profile.sms_preference === null){
      this.profile.sms_preference = false
    }
    if(this.profile.email_preference === null){
      this.profile.email_preference = false
    }
  }

  dismiss() {
    (this.el.closest('ion-modal') as any).dismiss();
  }

  edit() {
    if (this.phone === undefined || this.phone === null){
      if (this.picture === undefined) {
        this.picture = this.profile.picture
      }
      this.profile = {id: this.profile.id,
        first_name: this.first_name.value,
        last_name: this.last_name.value,
        email: this.email.value,
        user_id: this.profile.user_id,
        sms_preference: this.smsToggle.checked,
        email_preference: this.emailToggle.checked,
        picture: this.picture
      };
    } else {
        if (this.picture === undefined) {
          this.picture = this.profile.picture
        }
        this.profile = {id: this.profile.id,
          first_name: this.first_name.value,
          last_name: this.last_name.value,
          email: this.email.value,
          phone: this.phone.value,
          user_id: this.profile.user_id,
          sms_preference: this.smsToggle.checked,
          email_preference: this.emailToggle.checked,
          picture: this.picture
        };
    }
    (this.el.closest('ion-modal') as any).dismiss(this.profile,
      'save-profile');
  }

  otherFormValidation() {
    let inputs = [this.first_name, this.last_name, this.email, this.phone]
    let notes = [this.firstNameError, this.lastNameError, this.emailError, this.smsError]
    let err = 0
    for (let i = 0; i < 4; i++) {
      if (inputs[i] === this.email) {
        let emailRegEx = /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/;
        {
          if (!emailRegEx.test(this.email.value)) {
            inputs[i].classList.add('failLine')
            notes[i].classList.add('visibleMessage')
            err += 1
          } else if (inputs[i].classList.contains('failLine')) {
            inputs[i].classList.remove('failLine');
            notes[i].classList.remove('visibleMessage');
          }
        }
      } else if (inputs[i] === this.phone) {
        if (this.smsToggle.checked === true && (this.phone.value === '' || this.phone.value === null)) {
          inputs[i].classList.add('failLine');
          notes[i].classList.add('visibleMessage');
          err += 1
        } else if (inputs[i].classList.contains('failLine')) {
          inputs[i].classList.remove('failLine');
          notes[i].classList.remove('visibleMessage');
        }
      } else {
        if (inputs[i].value === '' || inputs[i].value === undefined) {
          inputs[i].classList.add('failLine');
          notes[i].classList.add('visibleMessage');
          err += 1
        } else if (inputs[i].classList.contains('failLine')) {
          inputs[i].classList.remove('failLine');
          notes[i].classList.remove('visibleMessage');
        }
      }
    }
    if (err === 0) {
      this.edit()
    }
  }

  @Listen("onUploadCompleted")
  imageFile(event) {
    this.picture = event.detail.result;
  }

  pictureValueChanged(event) {
    this.oldPicture = this.profile.picture;
    this.picture = event.target.value;
  }

  toggleSms() {
    if (this.smsToggle.checked){
      this.phoneItem.classList.add('hidden')
    }else{
      this.phoneItem.classList.remove('hidden')
    }
  }

  phoneInputIsHidden() {
    if(this.profile.sms_preference){
      return ''

    }else{
      return 'hidden'
    }
  }

  renderEmail() {
    return [
        <ion-item>
          <ion-label position={"floating"} color="medium" style={{fontSize: "18px"}}>
                Email Address
          </ion-label>
          <ion-input
            value={this.profile.email}
            ref={el => this.email = el as HTMLIonInputElement}
            class={'passLine'}
          />
          <ion-note class={'hiddenMessage'} ref={el => this.emailError = el as HTMLIonNoteElement}>
            The email address you entered is invalid.
          </ion-note>
        </ion-item>
      ]
  }

  renderSms() {
       return [
         <ion-item class={this.phoneInputIsHidden()} ref={el => this.phoneItem = el as HTMLIonItemElement}>
           <ion-label position={"floating"} color="medium" style={{fontSize: "18px"}}>
                 Phone Number
           </ion-label>
           <ion-input
             value={this.profile.phone}
             ref={el => this.phone = el as HTMLIonInputElement}
             class={'passLine'}
           />
           <ion-note class={'hiddenMessage'} ref={el => this.smsError = el as HTMLIonNoteElement}>
             You must provide a phone number to communicate via text.
           </ion-note>
         </ion-item>
       ]
  }

  renderPicture() {
    if (this.profile.picture === "") {
      this.profile.picture = this.avatarBase + `${this.profile.first_name}+${this.profile.last_name}`;
      return this.profile.picture
    } else if (this.picture != undefined && this.picture != '') {
      return this.picture
    } else if (this.profile.picture != this.oldPicture) {
      return this.profile.picture
    } else {
      this.profile.picture = this.avatarBase + `${this.profile.first_name}+${this.profile.last_name}`;
      return this.profile.picture
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>
            My Profile
          </ion-title>
          <ion-buttons slot={'end'}>
            <ion-button color={"tertiary"} onClick={()=>{this.dismiss()}}>
              <ion-label id={'cancel'}>
                Cancel
              </ion-label>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'}>
        <ion-grid>
          <ion-row justify-items-center={true} align-items-center={true}>
            <ion-col align-self-center={true}>
              <ion-card>
                <ion-list lines="none">
                  <ion-item text-center={true}>
                    <ion-label>
                      <img src={this.renderPicture()} style={{
                        height: "72px",
                        width: "72px",
                        marginLeft: "auto",
                        marginRight: "auto",
                        borderRadius: "50%"
                      }}
                      />
                    </ion-label>
                  </ion-item>
                  <ion-item>
                    <ion-label position={"floating"} color="medium" style={{fontSize: "18px"}}>
                          First Name
                    </ion-label>
                    <ion-input
                      value={this.profile.first_name}
                      ref={el => this.first_name = el as HTMLIonInputElement}
                      class={'passLine'}
                    />
                    <ion-note class={'hiddenMessage'} ref={el => this.firstNameError = el as HTMLIonNoteElement}>
                      First Name is required
                    </ion-note>
                  </ion-item>
                  <ion-item>
                    <ion-label position={"floating"} color="medium" style={{fontSize: "18px"}}>
                          Last Name
                    </ion-label>
                    <ion-input
                      value={this.profile.last_name}
                      ref={el => this.last_name = el as HTMLIonInputElement}
                      class={'passLine'}
                    />
                    <ion-note class={'hiddenMessage'} ref={el => this.lastNameError = el as HTMLIonNoteElement}>
                      Last Name is required
                    </ion-note>
                  </ion-item>
                  <ion-item>
                    <ion-label position={"stacked"} style={{paddingBottom: "18px"}}>
                      <p>
                        <ion-text color="medium" style={{fontSize: "18px"}}>
                          Profile Picture
                        </ion-text>
                      </p>
                    </ion-label>
                    <image-upload />
                    <ion-textarea
                      rows={5}
                      onChange={(event) => { this.pictureValueChanged(event) }}
                      value={this.renderPicture()}
                    />
                  </ion-item>
                  {this.renderEmail()}
                  {this.renderSms()}
                  <ion-item lines="full">
                    <ion-label>
                        <ion-text color="medium" style={{fontSize: "12px"}}>
                          COMMUNICATION SETTINGS
                        </ion-text>
                    </ion-label>
                  </ion-item>
                  <ion-item>
                    <ion-text color="medium" style={{fontSize: "13px"}}>
                      SMS
                    </ion-text>
                    <ion-toggle name="smsToggle" slot="end" color="success" ref={el => this.smsToggle = el as HTMLIonToggleElement} checked={this.profile.sms_preference} onClick={() => {this.toggleSms()}}/>
                  </ion-item>
                  <ion-item>
                    <ion-text color="medium" style={{fontSize: "13px"}}>
                      E-Mail
                    </ion-text>
                    <ion-toggle name="emailToggle" slot="end" color="success" ref={el => this.emailToggle = el as HTMLIonToggleElement} checked={this.profile.email_preference}/>
                  </ion-item>
                  <ion-item lines="none" padding-top={true} padding-bottom={true}>
                    <ion-button expand={"full"} color={'secondary'} style={{
                      width: "100%",
                      height: "100%"
                    }} onClick={()=>{this.otherFormValidation()}}>
                      <ion-label>
                        SAVE CHANGES
                      </ion-label>
                    </ion-button>
                  </ion-item>
                </ion-list>
              </ion-card>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
