import { Component, h, Element, Prop} from '@stencil/core';
import {AssignmentsHttpService} from "../../http_services/assignments.http_service";
import {format} from "date-fns";

@Component({
  tag: 'app-edit-assignment-modal',
})
export class AppEditAssignmentModal {

  @Prop() assignment:any;
  @Prop() assignmentId: any;
  @Prop() courseId: any;

  private router = document.querySelector('ion-router');

  name:HTMLIonInputElement;
  due_date: any;
  description:HTMLIonTextareaElement;
  possible_points:HTMLIonInputElement;
  nameError:HTMLIonNoteElement;
  dateError:HTMLIonNoteElement;
  descriptionError:HTMLIonNoteElement;
  pointsError:HTMLIonNoteElement;

  @Element() el: Element;

  dismiss(){
    (this.el.closest('ion-modal') as any).dismiss();
  }

  async updateAssignment(){
    this.assignment = {id: this.assignmentId, name: this.name.value, due_date: format(this.due_date.value, 'YYYY-MM-DD'),
      description: this.description.value, possible_points: this.possible_points.value, course_id: this.courseId};
    await new AssignmentsHttpService().update(this.assignment);
    (this.el.closest('ion-modal') as any).dismiss(this.assignment, 'update-assignment');
  }

  async deleteAssignment(){
    await new AssignmentsHttpService().delete(this.assignmentId);
    (this.el.closest('ion-modal') as any).dismiss({id:this.assignmentId}, 'delete-assignment');
    this.router.back();
  }

  validateForm() {
    let inputs = [this.name, this.due_date, this.description, this.possible_points];
    let notes = [this.nameError, this.dateError, this.descriptionError, this.pointsError];
    let err = 0;
    for (let i=0;i<4;i++){
      if (inputs[i].value === '' || inputs[i].value === undefined) {
        inputs[i].classList.add('failLine');
        notes[i].classList.remove('hiddenMessage');
        err +=1
      }
      else if (inputs[i].classList.contains('failLine')) {
        inputs[i].classList.remove('failLine');
        notes[i].classList.add('hiddenMessage');
      }
    }
    if (err === 0) {
      this.updateAssignment()
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>Edit Assignment</ion-title>
          <ion-buttons slot={'end'}>
            <ion-button color={"tertiary"} onClick={()=>{this.dismiss()}}>
              <ion-label>Cancel</ion-label>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'}>
        <ion-card>
          <ion-list lines={'none'}>
            <ion-item>
              <ion-label position={"floating"}>
                Assignment name
              </ion-label>
              <ion-input
                value={this.assignment.name}
                ref={el => this.name = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.nameError = el as HTMLIonNoteElement}>
                Name is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Due Date
              </ion-label>
              <ion-datetime
                min="2018"
                max="2030"
                value={this.assignment.due_date}
                ref={el => this.due_date = el as HTMLIonDatetimeElement}
                class={'passLine'}
              >
              </ion-datetime>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.dateError = el as HTMLIonNoteElement}>
                Due date is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Description
              </ion-label>
              <ion-textarea
                value={this.assignment.description}
                ref={el => this.description = el as HTMLIonTextareaElement}
                class={'passLine'}
              >
              </ion-textarea>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.descriptionError = el as HTMLIonNoteElement}>
                Assignment description is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Possible Points
              </ion-label>
              <ion-input
                value={this.assignment.possible_points}
                ref={el => this.possible_points = el as HTMLIonInputElement}
                class={'passLine'}
                type={'number'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.pointsError = el as HTMLIonNoteElement}>
                Possible points is required
              </ion-note>
            </ion-item>
          </ion-list>
            <ion-button strong={true} color={"secondary"} expand={"full"} onClick={()=>{this.validateForm()}}>
              <ion-label >
                SAVE
              </ion-label>
            </ion-button>
            <ion-button color={"white"} strong={true} expand={"full"} onClick={()=>{this.deleteAssignment()}}>
              <ion-label>
                DELETE
              </ion-label>
            </ion-button>
        </ion-card>
      </ion-content>
    ];
  }
}

