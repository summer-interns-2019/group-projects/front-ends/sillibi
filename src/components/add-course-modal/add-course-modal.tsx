import {Component, h, Element, Listen, State} from '@stencil/core';
import {UserHttpService} from '../../http_services/user.http_service';
import {popoverController} from "@ionic/core";

@Component({
  tag: 'add-course-modal',
})
export class AddCourseModal {
  name:HTMLIonInputElement;
  number:HTMLIonInputElement;
  section:HTMLIonInputElement;
  term:HTMLIonInputElement;
  instructor:HTMLIonInputElement;
  nameError:HTMLIonNoteElement;
  numberError:HTMLIonNoteElement;
  sectionError:HTMLIonNoteElement;
  termError:HTMLIonNoteElement;
  instructorError:HTMLIonNoteElement;

  @State() color = 'rgba(255, 185, 0, 1)';
  @Element() el: Element;

  @Listen('colorPicked')
  changeColor(event){
    this.color = event.detail;
  }
  dismiss(){
    (this.el.closest('ion-modal') as any).dismiss();
  }
  async addCourse(){
    const currentUser = await new UserHttpService().where({});
    const institutionId = currentUser.institution_id;

    let searchTerms = [this.number.value, this.section.value, this.term.value];
    let courseSearch = searchTerms.join("").split(" ").join("").toLowerCase();

    (this.el.closest('ion-modal') as any).dismiss({name: this.name.value, number: this.number.value, section: this.section.value,
      term: this.term.value, instructor: this.instructor.value, color: this.color, course_search:courseSearch, institution_id: institutionId, verified:false}, 'add-course-modal');
  }

  async presentPopover(ev) {

    const popover = await popoverController.create({
      component: 'course-search-popover',
      translucent: true,
      showBackdrop: true,
      event: ev,
      componentProps:{}
    });
    return await popover.present();
  }


  validateForm() {
    let inputs = [this.name, this.number, this.section, this.term, this.instructor];
    let notes = [this.nameError, this.numberError, this.sectionError, this.termError, this.instructorError];
    let err = 0;
    for (let i=0;i<5;i++){
      if (inputs[i].value === '') {
        inputs[i].classList.add('failLine');
        notes[i].classList.remove('hiddenMessage');
        err +=1
      } else if (inputs[i].classList.contains('failLine')) {
        inputs[i].classList.remove('failLine');
        notes[i].classList.add('hiddenMessage');
      }
    }
    if (err === 0) {
      this.addCourse()
    }
  }


  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot={'start'}>
            <ion-button onClick={()=>{this.presentPopover(event)}}>
              <ion-icon color={'tertiary'} name="search"></ion-icon>
            </ion-button>
          </ion-buttons>
          <ion-title>
            Add Course
          </ion-title>
          <ion-buttons slot={'end'}>
            <ion-button onClick={()=>{this.dismiss()}}>
              <ion-label color={'tertiary'}>
                Cancel
              </ion-label>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'}>
        <ion-popover-controller/>
        <ion-card>
          <ion-list lines={'none'}>
            <ion-item>
              <ion-label position={"floating"}>
                Course name *
              </ion-label>
              <ion-input
                placeholder={'Business administration'}
                ref={el => this.name = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.nameError = el as HTMLIonNoteElement}>
                Name is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Course number *
              </ion-label>
              <ion-input
                placeholder={'e.g., MGMT 100'}
                ref={el => this.number = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.numberError = el as HTMLIonNoteElement}>
                Course number is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Section number *
              </ion-label>
              <ion-input
                placeholder={'e.g., 001'}
                ref={el => this.section = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.sectionError = el as HTMLIonNoteElement}>
                Section number is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Term *
              </ion-label>
              <ion-input
                placeholder={'Fall 2017'}
                ref={el => this.term = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.termError = el as HTMLIonNoteElement}>
                Term is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Instructor *
              </ion-label>
              <ion-input placeholder={'Dr. Sam Birk'}
                         ref={el => this.instructor = el as HTMLIonInputElement}
                         class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.instructorError = el as HTMLIonNoteElement}>
                Instructor name is required
              </ion-note>
            </ion-item>
            <color-grid color={this.color}/>
            <ion-button expand={"full"} onClick={()=>{this.validateForm()}} color={'secondary'}>
              <ion-label>
                ADD COURSE
              </ion-label>
            </ion-button>
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}
