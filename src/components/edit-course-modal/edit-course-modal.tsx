import {Component, Element, h, Listen, Prop, State} from '@stencil/core';

@Component({
  tag: 'edit-course-modal',
})
export class EditCourseModal {
  name:HTMLIonInputElement;
  number:HTMLIonInputElement;
  section:HTMLIonInputElement;
  term:HTMLIonInputElement;
  instructor:HTMLIonInputElement;
  nameError:HTMLIonNoteElement;
  numberError:HTMLIonNoteElement;
  sectionError:HTMLIonNoteElement;
  termError:HTMLIonNoteElement;
  instructorError:HTMLIonNoteElement;
  @Prop() course:any;
  @State() color: string = this.course.color;
  @Element() el: Element;

  dismiss(){
    (this.el.closest('ion-modal') as any).dismiss();
  }
  @Listen('colorPicked')
  changeColor(event){
    this.color = event.detail;
  }
  edit(){
    this.course = {id: this.course.id, name: this.name.value, number: this.number.value, section: this.section.value,
    term: this.term.value, instructor: this.instructor.value, color: this.color};
    (this.el.closest('ion-modal') as any).dismiss(this.course,
      'save-changes');
  }
  remove(){
    (this.el.closest('ion-modal') as any).dismiss(this.course,
      'remove');
  }

  validateForm() {
    let inputs = [this.name, this.number, this.section, this.term, this.instructor];
    let notes = [this.nameError, this.numberError, this.sectionError, this.termError, this.instructorError];
    let err = 0;
    for (let i=0;i<5;i++){
      if (inputs[i].value === '') {
        inputs[i].classList.add('failLine');
        notes[i].classList.remove('hiddenMessage');
        err += 1
      } else if (inputs[i].classList.contains('failLine')) {
        inputs[i].classList.remove('failLine');
        notes[i].classList.add('hiddenMessage');
      }
    }
    if (err === 0) {
      this.edit()
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>
            Edit Course
          </ion-title>
          <ion-buttons slot={'end'}>
            <ion-button color={"tertiary"} onClick={()=>{this.dismiss()}}>
              <ion-label id={'cancel'}>
                Cancel
              </ion-label>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'}>
        <ion-card>
          <ion-list lines={'none'}>
            <ion-item>
              <ion-label position={"floating"}>
                Course name
              </ion-label>
              <ion-input
                value={this.course.name}
                ref={el => this.name = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.nameError = el as HTMLIonNoteElement}>
                Name is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Course number
              </ion-label>
              <ion-input
                value={this.course.number}
                ref={el => this.number = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.numberError = el as HTMLIonNoteElement}>
                Course number is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Section number
              </ion-label>
              <ion-input
                value={this.course.section}
                ref={el => this.section = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.sectionError = el as HTMLIonNoteElement}>
                Section number is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Term
              </ion-label>
              <ion-input
                value={this.course.term}
                ref={el => this.term = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.termError = el as HTMLIonNoteElement}>
                Term is required
              </ion-note>
            </ion-item>
            <ion-item>
              <ion-label position={"floating"}>
                Instructor
              </ion-label>
              <ion-input
                value={this.course.instructor}
                ref={el => this.instructor = el as HTMLIonInputElement}
                class={'passLine'}
              >
              </ion-input>
              <ion-note class={'hiddenMessage visibleMessage'} ref={el => this.instructorError = el as HTMLIonNoteElement}>
                Instructor name is required
              </ion-note>
            </ion-item>
            <color-grid color={this.color}/>
          </ion-list>
            <ion-button expand={"full"} color={'secondary'} onClick={()=>{this.validateForm()}}>
              <ion-label>
                SAVE CHANGES
              </ion-label>
            </ion-button>
            <ion-button color={'white'} expand={"full"} onClick={()=>{this.remove()}} href={'/courses'}>
              <ion-label color={'medium'}>
                DROP COURSE
              </ion-label>
            </ion-button>
        </ion-card>
      </ion-content>
    ];
  }
}
