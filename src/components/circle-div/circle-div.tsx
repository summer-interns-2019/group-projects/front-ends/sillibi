import {Component, h, Prop, Event, EventEmitter} from '@stencil/core';

@Component({
  tag: 'circle-div',
})
export class CircleDiv {
  @Prop() color: string;
  @Prop() selected:boolean = false;
  @Event() colorPicked:EventEmitter;
  innerCircleStyle = {
    borderRadius: "50%",
    height: '25px',
    width: '25px',
    backgroundColor: this.color,
    marginLeft: '50%',
    marginTop: '50%',
    transform: "translate(-50%, -50%)"
  };
  chooseColor(){
    if (this.selected) {return}
    this.colorPicked.emit(this.color);
  }
  outerCircleStyle(){
    let style = {
      borderRadius: "50%",
      height: '30px',
      width: '30px',
      backgroundColor: 'white',
      border: 'solid black 1px'
    };
    if (this.selected){
      return style
    } else {
      return {...style, border: 'solid white 1px'}
    }
  }
  render(){
      return [
        <div style={this.outerCircleStyle()}>
          <div style={this.innerCircleStyle} onClick={()=>{this.chooseColor()}}/>
        </div>
      ]
  }
}
