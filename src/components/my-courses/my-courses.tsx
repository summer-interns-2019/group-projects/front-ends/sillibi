import {Component, h, State, Listen, Element} from '@stencil/core';
import {CourseHttpService} from '../../http_services/course.http_service';
import {CurrentCoursesHttpService} from "../../http_services/current_courses.http_service"
import {ProfileHttpService} from "../../http_services/profile.http_service"
import {CoursesUsersHttpService} from "../../http_services/courses_users.http_service"
import _ from 'underscore';
import {modalController} from "@ionic/core";

@Component({
  tag: 'my-courses',
})
export class MyCourses {
  @State() toggle:boolean = false;
  @State() courses:any[];
  @State() modal:any;
  @State() profile;
  @State() associations:any[];
  @Element() el: Element;
  private router: HTMLIonRouterElement = document.querySelector('ion-router');


  @Listen('ionModalWillDismiss', {target: 'body'})
  async update(event) {
    if (event.detail.role === 'add-course-modal') {
      this.addCourseToDbIfNew(event.detail.data)
    } else if (event.detail.role === 'save-changes') {
      this.saveEdit(event.detail.data)
    } else if (event.detail.role === 'remove'){
      this.deleteCourse(event.detail.data)
    }
  }

  async addCourseToDbIfNew(data) {
    //check to see if entered course already exists at User's institution using course search handle
    const existingCourse = await new CourseHttpService().where({course_search:data.course_search, institution_id:data.institution_id})
    console.log(existingCourse)
    if (existingCourse[0] === undefined) {
      //course does not exist--create new course
      const response = await new CourseHttpService().create(data);
      this.addUserToCourse(response)
    } else{
      //course already exists--just put student in existing course
      this.addUserToCourse(existingCourse[0])
    }
  }

  async addUserToCourse (course) {
    this.courses = [...this.courses, course];
    let newAssociation = await new CoursesUsersHttpService().create({course_id:course.id});
    this.associations = [...this.associations, newAssociation]
  }


  async saveEdit(data) {
    const oldRecord = _.findWhere(this.courses, {id: data.id});
    const index = _.findIndex(this.courses, oldRecord);
    this.courses[index] = data;
    await new CourseHttpService().update(this.courses[index]);
    this.courses = [...this.courses];
  }
  async deleteCourse(data) {
    const removedCourse = _.findWhere(this.courses, {id: data.id});
    const removedAssociation = _.findWhere(this.associations, {course_id: data.id});
    await new CoursesUsersHttpService().delete(removedAssociation.id);
    this.courses = _.without(this.courses, removedCourse);
    this.associations = _.without(this.associations, removedAssociation);
  }

  async componentWillLoad() {
    this.courses = await new CurrentCoursesHttpService().where({});
    this.profile = await new ProfileHttpService().where({});
    this.associations = await new CoursesUsersHttpService().where({user_id:this.profile.id});
    console.log(this.courses);
    console.log(this.associations);
    this.router.addEventListener("ionRouteDidChange", async(event: any) => {
      if (event.detail.from === null ) { return }
      if(event.detail.from.startsWith("/search-course-info/") && event.detail.to === ("/courses")){
        this.courses = await new CurrentCoursesHttpService().where({});
        this.associations = await new CoursesUsersHttpService().where({user_id:this.profile.id});
      }
    });
  }

  async openAddCourseModal() {
    this.modal = await modalController.create({
      component: 'add-course-modal',
      componentProps: {}
    });
    await this.modal.present();
  }

  openCourseSearch(strLoc) {
    this.router.push(strLoc, 'forward');
  }

  renderCourses() {
    return this.courses.map((course) => {
      return [
        <course-display course={course}/>
      ]
    })
  }
  renderContent(){
    if (this.courses === undefined || this.courses.length === 0) {
      return [
        <ion-grid>
          <ion-row>
            <ion-col>
              <img src={'assets/icon/illoAddCourse.svg'} onClick={()=>{this.openAddCourseModal()}} alt={'add course'}/>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col>
                <ion-label>
                  Add a course to upload a syllabus.
                </ion-label>
            </ion-col>
          </ion-row>
        </ion-grid>
      ];
    } else {
      return [
        this.renderCourses(),
        <ion-grid>
          <ion-row>
            <ion-col style={{textAlign: 'center'}}>
              <img src={'assets/icon/BTN_AddCourse.svg'} onClick={()=>{this.openAddCourseModal()}} alt={'add course'}/>
            </ion-col>
          </ion-row>
        </ion-grid>
      ]
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot={'start'}>
            <ion-button onClick={()=>{this.openCourseSearch(`/course-search/${this.profile.user_id}`)}}>
              <ion-icon color={'tertiary'} name="search"></ion-icon>
            </ion-button>
          </ion-buttons>
          <ion-title>
            My Courses
          </ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={'light'} text-center={true}>
        {this.renderContent()}
      </ion-content>,
      <app-nav-tabs/>
    ];
  }
}

