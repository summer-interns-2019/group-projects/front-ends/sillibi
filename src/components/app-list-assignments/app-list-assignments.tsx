import { Component, h, Prop, } from '@stencil/core';

@Component({
  tag: 'app-list-assignments',
})
export class AppListAssignments {

  @Prop() assignment: any;

  private router = document.querySelector('ion-router');

  assignmentClickedToView(){
    this.router.push(`/view-assignment/${this.assignment.id}`)
  }

  render() {
    return [
      <ion-item lines={"none"} onClick={() => this.assignmentClickedToView()}>
        <ion-label>
          <h2>{this.assignment.name}</h2>
        </ion-label>
        <ion-note color={"dark"} slot={"end"}>{this.assignment.possible_points} pts</ion-note>
      </ion-item>,
      <ion-item onClick={() => this.assignmentClickedToView()}>
        <p style={{marginTop: "0", maxHeight: "80px"}} class={"ion-text-wrap"}>
          <ion-note>{this.assignment.description}</ion-note>
        </p>
      </ion-item>
    ];
  }
}
