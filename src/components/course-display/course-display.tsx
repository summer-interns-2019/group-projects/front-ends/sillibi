import {Component, h, Prop, State} from '@stencil/core';
import {SyllabusesHttpService} from "../../http_services/syllabuses.http_service";

@Component({
  tag: 'course-display',
})
export class CourseDisplay {
  @Prop() course: any;
  @State() assignments: any[];
  @State() syllabi: any[];
  private router: HTMLIonRouterElement = document.querySelector('ion-router');

  pushNewLink(strLoc) {
    this.router.push(strLoc, 'forward');
  }

  async componentWillLoad(){
    this.syllabi = await new SyllabusesHttpService().where({});
  }

  syllabusIcon() {
    let isPresent = this.syllabi.some(syllabus => syllabus.course_id === this.course.id);
    if (isPresent) {
      return [
        <ion-icon slot="end" name="arrow-forward" color="tertiary"></ion-icon>
      ]
    } else {
      return [
        <ion-icon slot="end" name="add-circle-outline" color="tertiary"></ion-icon>
      ]
    }
  }

  render() {
    return [
      <ion-card>
        <ion-list>
          <ion-item
            lines={'none'}
            style={{borderBottom:`1px solid ${this.course.color}`}}
            href={`/course-info/${this.course.id}`}
          >
            <ion-label>
              {this.course.name}
            </ion-label>
          </ion-item>
          <ion-item lines={'full'}>
            <ion-label>
              <ion-note>
                {this.course.number} - {this.course.section} <br/> {this.course.instructor}
              </ion-note>
            </ion-label>
          </ion-item>
          <ion-item lines={'full'} onClick={_ => this.pushNewLink(`/syllabuses/${this.course.id}`)}>
              <ion-icon slot={'start'} name={'copy'} color={'tertiary'}/>
            <ion-label>
              Syllabus
            </ion-label>
            {/*<ion-badge style={{backgroundColor: 'white', color:'grey'}}>*/}
            {/*  0*/}
            {/*</ion-badge>*/}
            <ion-buttons>
              <ion-button href={`/syllabuses/${this.course.id}`} >
                {this.syllabusIcon()}
              </ion-button>
            </ion-buttons>
          </ion-item>
          <ion-item lines={'none'} onClick={() => this.pushNewLink(`/assignments/${this.course.id}`)}>
              <ion-icon slot={'start'} name={'paper'} color={'tertiary'}/>
            <ion-label>
              Assignments
            </ion-label>
            {/*<ion-badge style={{backgroundColor: 'white', color:'grey'}}>*/}
            {/*  0*/}
            {/*</ion-badge>*/}
            <ion-buttons>
              <ion-button onClick={() => this.pushNewLink(`/assignments/${this.course.id}`)}>
                <ion-icon slot={'end'} name={'add-circle-outline'} color={'tertiary'}/>
              </ion-button>
            </ion-buttons>
          </ion-item>
        </ion-list>
      </ion-card>
    ];
  }
}
