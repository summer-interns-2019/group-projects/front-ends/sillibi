import {Component, h, Prop, State, Listen} from '@stencil/core';
import {ProfileHttpService} from "../../http_services/profile.http_service";
import {LogoutHttpService} from "../../http_services/logout.http_service";
import {modalController} from "@ionic/core";

@Component({
  tag: 'app-profile-page',
})
export class AppProfilePage {
  @Prop() profileId;
  @State() profile;
  @State() user;
  @State() modal:any;
  avatarBase = "https://ui-avatars.com/api/?rounded=true&background=32134e&color=fff&bold=true&size=116&name=";

  private router = document.querySelector('ion-router');

  async componentWillLoad() {
    this.profile = await new ProfileHttpService().where({});
  }

  @Listen('ionModalWillDismiss', {target: 'body'})
  async update(event) {
    if (event.detail.role === 'save-profile') {
      this.saveEdit(event.detail.data)
    }
  }

  async saveEdit(data) {
    // delete data.email;
    this.profile = data;
    let newProfile =  await new ProfileHttpService().update(this.profile);
    console.log(newProfile);
  }

  async openModal() {
    this.modal = await modalController.create({
      component: 'edit-profile-modal',
      componentProps: {profile:this.profile}
    });
    await this.modal.present();
  }

  async logout() {
    await new LogoutHttpService().delete();
    localStorage.clear();
    this.router.push('/')
  }

  renderPicture() {
    if (this.profile.picture === "") {
      this.profile.picture = this.avatarBase + `${this.profile.first_name}+${this.profile.last_name}`;
      return this.profile.picture
    } else {
      return this.profile.picture
    }
  }

  renderPhone() {
    if (this.profile.phone === null || this.profile.phone === '' || this.profile.phone === undefined) {
      return
    } else {
      return [
        <ion-item>
          <ion-label>
            <p>
              <ion-text color={'medium'}>
                Phone Number
              </ion-text>
            </p>
            <h2>
              {this.profile.phone}
            </h2>
          </ion-label>
        </ion-item>
      ]
    }
  }


  smsIconColor(){
    if (this.profile.sms_preference) {
      return 'primary'
    } else {
      return 'medium'
    }
  }


  emailIconColor(){
    if (this.profile.email_preference) {
      return 'primary'
    } else {
      return 'medium'
    }
  }

  render() {
      return [
        <ion-header>
          <ion-toolbar>
            <ion-title>
              My Profile
            </ion-title>
            <ion-buttons slot={'end'}>
              <ion-button color={"tertiary"} onClick={()=>{this.openModal()}}>
                <ion-label id={'cancel'}>
                  Edit
                </ion-label>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>,
        <ion-content color={'light'}>
          <ion-grid>
            <ion-row justify-items-center={true} align-items-center={true}>
              <ion-col align-self-center={true}>
                <ion-card>
                  <ion-list lines={'none'}>
                    <ion-item text-center={true}>
                      <ion-label>
                       <img src={this.renderPicture()} style={{
                         height: "72px",
                         width: "72px",
                         marginLeft: "auto",
                         marginRight: "auto",
                         borderRadius: "50%"
                       }}
                       />
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-label>
                        <p>
                          <ion-text color={'medium'}>
                            First Name
                          </ion-text>
                        </p>
                        <h2>
                          {this.profile.first_name}
                        </h2>
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-label>
                        <p>
                          <ion-text color={'medium'}>
                            Last Name
                          </ion-text>
                        </p>
                        <h2>
                          {this.profile.last_name}
                        </h2>
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-label>
                        <p>
                          <ion-text color={'medium'}>
                            Institution
                          </ion-text>
                        </p>
                        <h2>
                          {this.profile.institution}
                        </h2>
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-label>
                        <p>
                          <ion-text color={'medium'}>
                            Email Address
                          </ion-text>
                        </p>
                        <h2>
                          {this.profile.email}
                        </h2>
                      </ion-label>
                    </ion-item>
                    {this.renderPhone()}
                    <ion-item lines="full">
                      <ion-label>
                        <p>
                          <ion-text color="medium" style={{fontSize: "12px"}}>
                            COMMUNICATION SETTINGS
                          </ion-text>
                        </p>
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-button fill="clear" disabled={true}>
                        <ion-icon slot='start' name="mail" color={this.emailIconColor()} style={{ fontSize: "32px"}}/>
                      </ion-button>
                      <ion-button fill="clear" disabled={true}>
                        <ion-icon name="text" color={this.smsIconColor()}  style={{fontSize: "32px"}}/>
                      </ion-button>
                    </ion-item>
                    <ion-item lines="none" padding-top={true} padding-bottom={true}>
                      <ion-button expand={"full"} color={'secondary'} onClick={() => { this.logout() }}
                                  style={{
                        width: "100%",
                        height: "100%"
                      }}>
                        <ion-label>
                          LOGOUT
                        </ion-label>
                      </ion-button>
                    </ion-item>
                  </ion-list>
                </ion-card>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>,
        <app-nav-tabs/>
      ];
  }
}
