import { Component, h, State, Listen } from '@stencil/core';
import {LoginHttpService} from "../../http_services/login.http_service";

@Component({
  tag: 'app-login-page',
  styleUrl: 'app-login-page.css'
})
export class AppLoginPage {
  user: {email: string, password: string};
  @State() emailColor = 'medium';
  @State() passColor = 'medium';

  componentWillLoad() {
    if(localStorage.token !== undefined){
      location.hash = '/courses'
    }
    this.user = {email: '', password: ''}
  }

  async login(user) {
    try {
      const response = await new LoginHttpService().create({user: {email: user.email, password: user.password}});
      if (response != null) {
        location.hash = '/courses'
      }
    } catch(err) {
      alert("Invalid email or password. Please try again.")
    }
  }

  emailValueChanged(event) {
    this.user.email = event.target.value;
  }

  passwordValueChanged(event) {
    this.user.password = event.target.value
  }
  @Listen('ionFocus')
  yellowLabel(event){
    if (event.target.name === 'email') {
      this.emailColor = 'secondary'
    } else if (event.target.name === 'password') {
      this.passColor = 'secondary'
    }
  }

  @Listen('ionBlur')
  greyLabel(event){
    if (event.target.value === ''){
      if (event.target.name === 'email') {
        this.emailColor = 'medium'
      } else if (event.target.name === "password") {
        this.passColor = 'medium'
      }
    }
  }

  render() {
    return [
      <ion-content color="primary" text-center={true}>
        <ion-grid>
          <ion-row class="ion-align-items-end" style={{height:'50vh'}}>
            <ion-col>
              <ion-item color="primary">
                <ion-label color={this.emailColor} position="floating">
                  Email Address
                </ion-label>
                <ion-input
                  name={'email'}
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
                  onInput={(event) => { this.emailValueChanged(event)}}
                />
              </ion-item>
              <ion-item color="primary">
                <ion-label color={this.passColor} position="floating">
                  Password
                </ion-label>
                <ion-input
                  name={'password'}
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
                  type="password"
                  onInput={(event) => { this.passwordValueChanged(event)}}
                />
              </ion-item>
            </ion-col>
          </ion-row>
          <ion-row style={{textAlign: 'center', height:'20vh'}}>
            <ion-col>
              <ion-button expand="full" color="secondary" onClick={() => { this.login(this.user) }}>
                LOGIN
              </ion-button>
            </ion-col>
          </ion-row>
          <ion-row style={{textAlign: 'center', height:'20vh'}}>
            <ion-col>
              <ion-label>
              <p>
                Don't have an account?
              </p>
              </ion-label>
              <ion-button href="/register">
                <ion-text color="tertiary">
                  REGISTER
                </ion-text>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
