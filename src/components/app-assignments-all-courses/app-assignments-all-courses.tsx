import {Component, h, State, Prop, Listen} from '@stencil/core';
import {AssignmentsHttpService} from "../../http_services/assignments.http_service";
import {CurrentCoursesHttpService} from "../../http_services/current_courses.http_service";
import _ from "underscore";

@Component({
  tag: 'app-assignments-all-courses',
})
export class AppAssignmentsAllCourses {

  @Prop() courseId: any;
  @State() assignments: any = [];
  @State() courses: any;
  modal: any;

  async componentWillLoad(){
    this.courses = await new CurrentCoursesHttpService().where({});
    this.courses.forEach(async (elem) => {
      let assignmentList = await new AssignmentsHttpService().where({course_id: elem.id});
      assignmentList.forEach((el) => {
        this.assignments = [...this.assignments, el]
      })
    });
  }

  private router = document.querySelector('ion-router');

  clickedBackButton(){
    this.router.push(`/courses`, "back")
  }

  goToCoursePage(){
    this.router.push(`/courses`, "back")
  }

  @Listen('ionModalWillDismiss', {target: 'body'})
  async update(event) {
    if (event.detail.role === 'update-assignment') {
      const oldAssignment = await _.findWhere(this.assignments, {id: event.detail.data.id});
      const index = _.findIndex(this.assignments, oldAssignment);
      let newCopy = [...this.assignments];
      newCopy[index] = event.detail.data;
      this.assignments = [...newCopy];
    } else if (event.detail.role === 'delete-assignment') {
      const removedAssignment = _.findWhere(this.assignments, {id: event.detail.data});
      this.assignments = _.without(this.assignments, removedAssignment);
    }
  }

  listDatesCoursenamesAndAssignments(){
    //create new array of assignments that includes related Course: color and name
    let assignmentsWithCourseColorAndName = this.assignments;
    assignmentsWithCourseColorAndName.map((assignment) => {
      let index = _.findIndex(this.courses, {id: assignment.course_id});
      let course =  this.courses[index];
      assignment.course_name = course.name;
      assignment.course_color = course.color;
    });
    // sort new assignments array by date, then group assignments by date, then convert dictionary output to array
    let sortedByDateAssignments = _.sortBy(assignmentsWithCourseColorAndName, 'due_date');
    let groupedByDate = _.groupBy(sortedByDateAssignments, 'due_date');
    let groupedAssignmentsArray = _.toArray(groupedByDate);
    return[
      groupedAssignmentsArray.map((assignmentGroup) => {
        let assignmentGroupDueDate = assignmentGroup[0]['due_date'];
        return [
          <app-list-dates-all-courses
            assignmentGroup = {assignmentGroup}
            assignmentGroupDueDate = {assignmentGroupDueDate}
          />
        ]
      })
    ]
  }

  renderAssignmentImage(){
    if(this.assignments.length === 0){
      return [
        <ion-grid>
          <ion-row class={"ion-justify-content-center"}>
            <ion-col size="10">
              <img src={"../assets/images/Illo-robot.svg"} onClick={() => {this.goToCoursePage()}}/>
            </ion-col>
            <p>Our robots are working hard uploading other assignments. Would you like to manually upload them?</p>
            <ion-text
              class={"ion-text-uppercase"}
              color="tertiary"
              onClick={() => this.goToCoursePage()}
            >
              Input Manual Assignments
            </ion-text>
          </ion-row>
        </ion-grid>
      ]
    }
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot={"start"}>
            <ion-button color={"tertiary"} onClick={() => this.clickedBackButton()}>
              <ion-icon name="arrow-back"/>
            </ion-button>
          </ion-buttons>
          <ion-title>Assignments-All Courses</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content color={"light"} text-center={true}>
        {this.listDatesCoursenamesAndAssignments()}
        {this.renderAssignmentImage()}
      </ion-content>
    ];
  }
}
