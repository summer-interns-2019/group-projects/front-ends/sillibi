import {Component, h, Prop, State} from '@stencil/core';
import {SyllabusesHttpService} from "../../http_services/syllabuses.http_service";

@Component({
  tag: 'course-syllabus',
  styleUrl: 'course-syllabus.css'
})
export class CourseSyllabus {
  @Prop() courseId: number;
  syllabusId: number;
  @State() pdfData: string = '';
  hasSyllabus: boolean = false;
  pdfjsLib = window['pdfjs-dist/build/pdf'];

  private router: HTMLIonRouterElement = document.querySelector('ion-router');
  // private canvas: HTMLCanvasElement;
  private fakeInputButton: HTMLIonButtonElement;
  private wrapper: HTMLDivElement;
  private toolbar: HTMLIonToolbarElement;
  windowWidth = screen.width;
  windowHeight = screen.height;

  async componentWillLoad() {
    console.log(this.router);
    let service = new SyllabusesHttpService();
    console.log(`Getting ${this.courseId}`);
    let syllabus = await service.where({course_id: this.courseId});
    this.hasSyllabus = syllabus !== undefined && syllabus.length !== 0 ;
    console.log(syllabus);
    if (this.hasSyllabus) {
      this.pdfData = atob(syllabus[0].data);
      this.syllabusId = syllabus[0].id;
    }

    // To be replaced later by Cordova screen rotation display adjustment
    let supportsOrientationChange = "onorientationchange" in window,
      orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

    window.addEventListener(orientationEvent, () => {
      this.windowWidth = screen.width;
      this.windowHeight = screen.height;
      this.createPDF();
    }, false);
  }

  async componentDidRender() {
    if (this.pdfData === '') { return; }
    this.createPDF();
    this.hasSyllabus = true;
  }

  async createPDF() {
    console.log('HERE!!');
    if (this.hasSyllabus) {
      while(this.wrapper.firstChild) {
        this.wrapper.removeChild(this.wrapper.firstChild);
      }
    }

    this.pdfjsLib.GlobalWorkerOptions.workerSrc = '/assets/pdfjs-2.0.943-dist/build/pdf.worker.js';

    // Asynchronous download of PDF
    let loadingTask = this.pdfjsLib.getDocument({data: this.pdfData});

    let pdf = await loadingTask.promise;
    console.log('PDF loaded');

    this.wrapper.style.width = '100%';
    let wrapperHeight = this.windowHeight - this.toolbar.offsetHeight - this.fakeInputButton.offsetHeight + 2;
    this.wrapper.style.height = wrapperHeight + 'px';

    for(let i = 1; i <= pdf.numPages; i++) {
      let page = await pdf.getPage(i);

      // console.log('Page loaded');
      let testGrab = page.getViewport(1);
      let pageWidth = testGrab.viewBox[2];
      let pageHeight = testGrab.viewBox[3];

      let hwRatio = pageHeight / this.windowWidth;
      let ratio = this.windowWidth / pageWidth;

      let scale = 4;
      let viewport = page.getViewport(scale);

      let canvas: HTMLCanvasElement = document.createElement('canvas') as HTMLCanvasElement;
      canvas.id = `page-canvas${i}`;
      let context = canvas.getContext('2d');
      canvas.height = viewport.height;
      canvas.width = viewport.width;
      canvas.style.width = (this.windowWidth - 20)+ 'px';
      canvas.style.height = (((this.windowWidth - 20) * hwRatio) * ratio) + 'px';
      canvas.style.marginTop = '10px';
      canvas.style.transform = 'translate(10px, 0)';
      this.wrapper.appendChild(canvas);

      let renderContext = {
        canvasContext: context,
        viewport: viewport
      };
      page.render(renderContext);
      // Statement above returns a object with a promise, renderTask.promise;
      console.log('Page rendered');
    }
  }

  inputButtonClicked() {
    document.getElementById('syllabus-input').click();
  }

  changedFile(event) {
    let input = event.target;
    let file = input.files[0];
    let reader  = new FileReader();
    reader.addEventListener("load", async () => {
      let base64Str: any = reader.result;
      let findIndex = base64Str.indexOf(',');
      let formattedBase64Str = base64Str.substring(findIndex + 1, base64Str.length);
      console.log(`Editing ${this.courseId}`);
      let tempArr = window.location.href.split('/');
      let courseId = Number(tempArr[tempArr.length -1]);
      console.log(courseId);
      let service = new SyllabusesHttpService();
      if (this.hasSyllabus) {
        service.update({id: this.syllabusId, data: formattedBase64Str});
      } else {
        let syllabus = await service.create({course_id: this.courseId, data: formattedBase64Str});
        this.syllabusId = syllabus.id;
      }
      this.pdfData = atob(formattedBase64Str);
      this.hasSyllabus = true;
    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }

  backButtonAction(input_class) {
    console.log(input_class);
    input_class.router.push('/courses', 'back');
  }

  removeButtonAction() {
    console.log('Delete Clicked!');
    this.pdfData = '';
    this.hasSyllabus = false;
    let service = new SyllabusesHttpService();
    service.delete(this.syllabusId);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="light" ref={el => this.toolbar = el as HTMLIonToolbarElement}>
          <ion-title text-center={true}> Syllabus Upload </ion-title>
          <ion-buttons slot='start'>
            <ion-button onClick={() => {this.backButtonAction(this)}} >
                <ion-icon style={{color: 'dodgerblue', fontSize: '20px'}} name="ios-arrow-back"/>
            </ion-button>
          </ion-buttons>
          {this.renderRemoveButton()}
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        {this.renderContent()}
      </ion-content>,
      <ion-footer>
        {this.renderFooter()}
      </ion-footer>
    ];
  }

  renderRemoveButton() {
    if (this.hasSyllabus) {
      return [
        <ion-buttons slot='end'>
          <ion-button>
            <ion-label color="danger" onClick={_ => this.removeButtonAction()}>
              Remove
            </ion-label>
          </ion-button>
        </ion-buttons>
      ];
    }
  }

  renderContent() {
    if (this.hasSyllabus) {
      return [
        <div>
          <div ref={el => this.wrapper = el as HTMLDivElement}
               style={{backgroundColor: 'gainsboro', overflow: 'scroll', paddingBottom: '10px'}}>
          </div>
        </div>
      ];
    } else {
      return [
        <ion-row>
          <div style={{marginLeft: '50%', marginTop: '60px', transform: 'translate(-50%, 0)'}}
               onClick={_ => this.inputButtonClicked()}>
            <img src="../assets/icon/Illo-robot.svg" />
            <p style={{textAlign: 'center', fontSize: '18px', width: '282px'}}>
              Add a syllabus and one of our
              <br/> robot elves will input your
              <br/> assignments for you</p>
            <p style={{ textAlign: 'center', fontSize: '16px', fontWeight: 'bold', width: '282px',
                        color: '#00a7cd'}}>
              ADD SYLLABUS
            </p>
          </div>
        </ion-row>
      ];
    }
  }

  renderFooter() {
    if (this.hasSyllabus) {
      return [
        <input type="file" name="Syllabus" accept=".pdf" style={{display: 'none'}} id="syllabus-input"
               onChange={ event => this.changedFile(event)} />,
        <ion-button color="secondary" style={{height: '50px', margin: '0px'}} expand="full"
                    onClick={ _ => this.inputButtonClicked()} data-cats={this.courseId}
                    ref={el => this.fakeInputButton = el as HTMLIonButtonElement} >
          <span style={{fontSize: '20px'}}> ADD A DIFFERENT FILE </span>
        </ion-button>
      ];
    } else {
      return [
        <input type="file" name="Syllabus" accept=".pdf" style={{display: 'none'}} id="syllabus-input"
               onChange={event => this.changedFile(event)} />
      ];
    }
  }
}
