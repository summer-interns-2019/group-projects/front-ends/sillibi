import {Component, h, Listen, State} from '@stencil/core';
import {RegisterHttpService} from "../../http_services/register.http_service";
import {LoginHttpService} from "../../http_services/login.http_service";
import {ProfileHttpService} from "../../http_services/profile.http_service";

@Component({
  tag: 'app-register',
})
export class AppRegister {
  user: {email: string, password: string, institution_id: number};
  @State() firstColor = 'medium';
  @State() lastColor = 'medium';
  @State() emailColor = 'medium';
  @State() passColor = 'medium';
  firstName: string;
  lastName: string;
  institution: any;
  avatarBase = "https://ui-avatars.com/api/?rounded=true&background=32134e&color=fff&bold=true&size=116&name=";

  componentWillLoad() {
    this.user = {email: '', password: '', institution_id: null}
  }

  @Listen('ionFocus')
  yellowLabel(event){
    if (event.target.name === 'first') {
      this.firstColor = 'secondary'
    } else if (event.target.name === 'last') {
      this.lastColor = 'secondary'
    } else if (event.target.name === 'email') {
      this.emailColor = 'secondary'
    } else if (event.target.name === 'password') {
      this.passColor = 'secondary'
    }
  }

  @Listen('ionBlur')
  greyLabel(event){
    if (event.target.value === ''){
      if (event.target.name === 'first') {
        this.firstColor = 'medium'
      } else if (event.target.name === "last") {
        this.lastColor = 'medium'
      } else if (event.target.name === 'email') {
        this.emailColor = 'medium'
      } else if (event.target.name === "password") {
        this.passColor = 'medium'
      }
    }
  }

  @Listen('sendInstitution')
  getInstitution(data){
    this.institution = data.detail;
    console.log(data.detail)
  }

  async register(user) {
    try {const response = await new RegisterHttpService().create({user: {
      email: user.email,
      password: user.password,
      institution_id: this.institution.id
    }});
      if (response.errors != null) {
        const error = response.errors[0].status;
        if (error === "400"){
          let allErrors = `email ${response.errors[0].detail.email}\npassword ${response.errors[0].detail.password}`;
          alert(allErrors)
        }
      } else {
        await new LoginHttpService().create({user: {
          email: user.email,
          password: user.password,
        }});
        await new ProfileHttpService().create({
          first_name: this.firstName,
          last_name: this.lastName,
          email: user.email,
          user_id: response.id,
          picture: this.avatarBase + `${this.firstName}+${this.lastName}`
        });
        window.location.hash = '/courses'
      }
    } catch (err) {
      alert("Something went wrong. Please try again.")
    }
  }

  emailValueChanged(event) {
    this.user.email = event.target.value;
  }

  passwordValueChanged(event) {
    this.user.password = event.target.value
  }

  firstNameValueChanged(event) {
    this.firstName = event.target.value;
  }

  lastNameValueChanged(event) {
    this.lastName = event.target.value
  }

  render() {
    return [
      <ion-content color="primary" text-center={true}>
        <ion-grid>
          <ion-row class="ion-align-items-end" style={{height:'55vh'}}>
            <ion-col>
              <ion-item color={'primary'} lines="none">
                <institution-list/>
              </ion-item>
              <ion-item color="primary" lines="none">
                <ion-label color={this.firstColor} position="floating">
                  First Name
                </ion-label>
                <ion-input
                  name='first'
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
                  onInput={(event) => { this.firstNameValueChanged(event) }}
                />
              </ion-item>
              <ion-item color="primary" lines="none">
                <ion-label color={this.lastColor} position="floating">
                  Last Name
                </ion-label>
                <ion-input
                  name='last'
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
                  onInput={(event) => { this.lastNameValueChanged(event) }}
                />
              </ion-item>
              <ion-item color="primary" lines="none">
                <ion-label color={this.emailColor} position="floating">
                  Email Address
                </ion-label>
                <ion-input
                  name='email'
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
                  onInput={(event) => { this.emailValueChanged(event) }}
                />
              </ion-item>
              <ion-item color="primary" lines="none">
                <ion-label color={this.passColor} position="floating">
                  Password
                </ion-label>
                <ion-input
                  name={'password'}
                  style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
                  type="password"
                  onInput={(event) => { this.passwordValueChanged(event) }}
                />
              </ion-item>
            </ion-col>
          </ion-row>
          <ion-row class="ion-align-items-center" style={{height:'20vh'}}>
            <ion-col>
              <ion-button expand="full" color="secondary" onClick={() => { this.register(this.user)}}>
                CREATE ACCOUNT
              </ion-button>
            </ion-col>
          </ion-row>
          <ion-row class="ion-align-items-center" style={{height:'20vh'}}>
            <ion-col>
              <ion-label>
                <p>
                  Already have an account?
                </p>
              </ion-label>
              <ion-button href="/login">
                <ion-text color="tertiary">
                  SIGN IN
                </ion-text>
              </ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
