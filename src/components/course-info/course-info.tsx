import {Component, h, Prop, State, Listen} from '@stencil/core';
import {CourseHttpService} from "../../http_services/course.http_service";
import {modalController} from "@ionic/core";

@Component({
  tag: 'course-info',
})
export class CourseInfo {
  @Prop() courseId;
  @State() course;
  @State() modal:any;

  async componentWillLoad() {
    this.course = await new CourseHttpService().find(this.courseId);
  }
  @Listen('ionModalWillDismiss', {target: 'body'})
  async save(event) {
    if (event.detail.role === 'save-changes') {
      this.course = event.detail.data
    }
  }
  async openModal() {
    this.modal = await modalController.create({
      component: 'edit-course-modal',
      componentProps: {course:this.course}
    });
    await this.modal.present();
  }
  render() {
      return [
        <ion-header>
          <ion-toolbar>
            <ion-title>
              Course information
            </ion-title>
            <ion-buttons slot={"start"}>
              <ion-button href={"/courses"}>
                <ion-icon color={'tertiary'} name={'arrow-back'}/>
              </ion-button>
            </ion-buttons>
            <ion-buttons slot={'end'}>
              <ion-button color={"tertiary"} onClick={()=>{this.openModal()}}>
                <ion-label id={'cancel'}>
                  Edit
                </ion-label>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>,
        <ion-content color={'light'}>
          <ion-card>
            <ion-list lines={'none'}>
              <ion-item style={{borderBottom:`1px solid ${this.course.color}`}}>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Course name
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.name}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Course number
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.number}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Section number
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.section}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Term
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.term}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item lines={'full'}>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Instructor
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.instructor}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item lines={'full'}>
                <ion-icon slot={'start'} color={'tertiary'} name={'copy'}/>
                <ion-label>
                  Syllabus uploads
                </ion-label>
                <ion-badge style={{backgroundColor: 'white', color:'grey'}}>
                  0
                </ion-badge>
              </ion-item>
              <ion-item >
                <ion-icon slot={'start'} color={'tertiary'} name={'paper'}/>
                <ion-label>
                  Assignments
                </ion-label>
                <ion-badge style={{backgroundColor: 'white', color:'grey'}}>
                  0
                </ion-badge>
              </ion-item>
            </ion-list>
          </ion-card>
        </ion-content>
      ];
  }
}
