import {Component, h, State, EventEmitter, Event} from '@stencil/core';
import {InstitutionHttpService} from "../../http_services/institution.http_service";

@Component({
  tag: 'institution-list',
})
export class InstitutionList {
  @State() institutions: any[];
  @State() toggle:boolean = false;
  @State() newSchool: any = '';
  addButton:HTMLIonButtonElement;
  input:HTMLIonInputElement;
  selector:HTMLIonSelectElement;
  @Event() sendInstitution:EventEmitter;

  async componentWillLoad() {
    this.institutions = await new InstitutionHttpService().where({});
    this.institutions = this.institutions.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
  }

  change(event){
    if (event.detail.value === "new" || event.target === this.addButton) {
      this.toggle = !this.toggle;
    } else {
      this.sendInstitution.emit(event.detail.value)
    }
  }

  async addInstitution(event){
    this.change(event);
    this.newSchool = this.input.value;
    const response = await new InstitutionHttpService().create({name: this.newSchool});
    this.institutions = [...this.institutions, response]
  }

  renderInstitutions() {
    return [this.institutions.map((institution) => {
      return [
        <ion-select-option value={institution}>{institution.name}</ion-select-option>
      ]
    }),
      <ion-select-option value={'new'}>
        Add another institution...
      </ion-select-option>
    ]
  }

  render() {
    if (this.toggle) {
      return [
        <ion-input
          style={{borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
          ref={el=>this.input = el as HTMLIonInputElement}
        />,
        <ion-buttons slot={"end"}>
          <ion-button
            onClick={(event) => {this.addInstitution(event)}}
            ref={el => this.addButton = el as HTMLIonButtonElement}
          >
            <ion-label>
              Add
            </ion-label>
          </ion-button>
        </ion-buttons>
      ]
    } else {
      return [
        <ion-label color="medium">Institution</ion-label>,
        <ion-select
          placeholder="Find your institution here"
          interface="action-sheet"
          style={{minWidth: '100%', borderBottom: "1px solid rgb(181, 181, 181, .2)"}}
          onIonChange={(event) => {this.change(event)}}
          value={this.newSchool}
        >
          {this.renderInstitutions()}
        </ion-select>
      ]
    }
  }
}
