import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'color-grid',

})
export class AppRoot {
  @Prop() color:string = 'rgba(255, 185, 0, 1)';
  colors = [
    ['rgba(255, 185, 0, 1)', 'rgba(231, 72, 86, 1)', 'rgba(0, 120, 215, 1)', 'rgba(0, 153, 188, 1)', 'rgba(145, 145, 145, 1)'],
    ['rgba(247, 99, 12, 1)', 'rgba(234, 0, 94, 1)', 'rgba(142, 140, 216, 1)', 'rgba(0, 183, 195, 1)', 'rgba(104, 118, 138, 1)'],
    ['rgba(218, 59, 1, 1)', 'rgba(227, 0, 140, 1)', 'rgba(135, 100, 184, 1)', 'rgba(0, 178, 148, 1)', 'rgba(86, 124, 115, 1)'],
    ['rgba(255, 67, 67, 1)', 'rgba(154, 0, 137, 1)', 'rgba(177, 70, 194, 1)', 'rgba(0, 204, 106, 1)', 'rgba(73, 130, 5, 1)']
  ];
  renderRows(){
    return this.colors.map((row)=>{
      return [
        <ion-row class={'ion-justify-content-center'}>
          {this.renderCols(row)}
        </ion-row>
      ]
    })
  }
  renderCols(row){
    return row.map((col)=>{
      if (col === this.color){
        return[
          <ion-col size={'2'}>
            <circle-div color={col} selected={true}/>
          </ion-col>
        ]} else {
        return [
          <ion-col size={'2'}>
            <circle-div color={col} selected={false}/>
          </ion-col>
        ]
      }
    })
  }
  render() {
    return [
      <ion-item lines={'none'}>
        <ion-label position={'stacked'}>
          Color
        </ion-label>
        <ion-grid style={{width:'100%'}}>
          {this.renderRows()}
        </ion-grid>
      </ion-item>
    ]
  }
}
