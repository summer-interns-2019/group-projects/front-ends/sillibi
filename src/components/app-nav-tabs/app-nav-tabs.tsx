import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-nav-tabs'
})
export class AppNavTabs {

  private router = document.querySelector('ion-router');

  clickedCoursesButton(){
    this.router.push(`/courses`)
  }

  clickedAssignmentsButton(){
    this.router.push(`/assignments-all`)
  }

  clickedProfileButton() {
    this.router.push('/profile')
  }

  render() {
    return [
      <ion-tab-bar color={"primary"}>
        <ion-tab-button onClick={() => this.clickedProfileButton()}>
          <ion-icon name="person-circle-outline"/>
        </ion-tab-button>
        <ion-tab-button onClick={() => this.clickedCoursesButton()}>
          <ion-icon name="copy-outline"/>
        </ion-tab-button>
        <ion-tab-button onClick={() => this.clickedAssignmentsButton()}>
          <ion-icon name="document-outline"/>
        </ion-tab-button>
        <ion-tab-button>
          <ion-icon name="ellipsis-vertical-outline"/>
        </ion-tab-button>
      </ion-tab-bar>
    ];
  }
}
