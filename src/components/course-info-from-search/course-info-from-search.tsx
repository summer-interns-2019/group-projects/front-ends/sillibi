import {Component, h, Prop, State} from '@stencil/core';
import {CourseHttpService} from "../../http_services/course.http_service";
import {CoursesUsersHttpService} from "../../http_services/courses_users.http_service";


@Component({
  tag: 'course-info-from-search',
})

export class CourseInfoFromSearch {
  // @Event() courseAddedFromSearch: EventEmitter;
  @Prop() courseId;
  @Prop() userId;
  @State() course;

  async componentWillLoad() {
    this.course = await new CourseHttpService().find(this.courseId);
  }

  async addUserToCourse () {
    await new CoursesUsersHttpService().create({user_id:this.userId, course_id:this.courseId});
    this.pushNewLink(`/courses`)
  }

  private router: HTMLIonRouterElement = document.querySelector('ion-router');
  pushNewLink(strLoc) {
    this.router.push(strLoc, 'forward');
  }

  render() {
      return [
        <ion-header>
          <ion-toolbar>
            <ion-title>
              Course information
            </ion-title>
            <ion-buttons slot={"start"}>
              <ion-button onClick={()=>{this.pushNewLink(`/course-search/${this.userId}`)}}>
                <ion-icon color={'tertiary'} name={'arrow-back'}/>
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>,
        <ion-content color={'light'}>
          <ion-card>
            <ion-list lines={'none'}>
              <ion-item style={{borderBottom:`1px solid ${this.course.color}`}}>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Course name
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.name}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Course number
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.number}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Section number
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.section}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Term
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.term}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item lines={'full'}>
                <ion-label>
                  <p>
                    <ion-text color={'medium'}>
                      Instructor
                    </ion-text>
                  </p>
                  <h2>
                    {this.course.instructor}
                  </h2>
                </ion-label>
              </ion-item>
              <ion-item lines={'full'}>
                <ion-icon slot={'start'} color={'tertiary'} name={'copy'}/>
                <ion-label>
                  Syllabus uploads
                </ion-label>
                <ion-badge style={{backgroundColor: 'white', color:'grey'}}>
                  0
                </ion-badge>
              </ion-item>
              <ion-item >
                <ion-icon slot={'start'} color={'tertiary'} name={'paper'}/>
                <ion-label>
                  Assignments
                </ion-label>
                <ion-badge style={{backgroundColor: 'white', color:'grey'}}>
                  0
                </ion-badge>
              </ion-item>
              <ion-button expand={"full"} onClick={()=>{this.addUserToCourse()}} color={'secondary'}>
                <ion-label>
                  ADD COURSE
                </ion-label>
              </ion-button>
            </ion-list>
          </ion-card>
        </ion-content>
      ];
  }
}
