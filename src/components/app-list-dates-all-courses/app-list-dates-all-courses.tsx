import {Component, h, Prop, State} from '@stencil/core';
import _ from 'underscore'
import {format, isThisWeek, isTomorrow, isToday, getDay, isPast} from "date-fns";

@Component({
  tag: 'app-list-dates-all-courses',
})
export class AppListDatesAllCourses {

  @Prop() assignmentGroupDueDate: any;
  @Prop() assignmentGroup: any;
  @State() assignmentGroupCourseColor: any;
  @State() assignmentGroupCourseName: any;

  private router = document.querySelector('ion-router');


  courseClickedToView(courseId){
    this.router.push(`/course-info/${courseId}`)
  }


  formatAssignmentGroupDueDate() {
    let today = new Date();
    if (isToday(this.assignmentGroupDueDate)) {
      return ['Today']
    } else if (isTomorrow(this.assignmentGroupDueDate)) {
      return ['Tomorrow']
    } else if (isThisWeek((this.assignmentGroupDueDate), {weekStartsOn: getDay(today)})) {
      return [format(this.assignmentGroupDueDate, 'dddd')]
    } else if (isPast(this.assignmentGroupDueDate)) {
      return [`Past due: ${format(this.assignmentGroupDueDate, 'MMMM DD, YYYY')}`]
    } else {
      return [format(this.assignmentGroupDueDate, 'MMMM DD, YYYY')]
    }
  }

  renderAssignmentsForAllCourses(assignmentGroup){
    return[
      assignmentGroup.map( (assignment) => {
        return[
          <app-list-assignments-all-courses assignment={assignment}/>
        ] } )
    ]
  }

  listCourseNameForAssignments(){
    let assignmentsGroupedByCourseName = _.groupBy(this.assignmentGroup, 'course_name');
    let assignmentsGroupedByCourseNameArray = _.toArray(assignmentsGroupedByCourseName);
    return[
      assignmentsGroupedByCourseNameArray.map( (assignmentGroup: any) => {
        this.assignmentGroupCourseName = assignmentGroup[0].course_name
        this.assignmentGroupCourseColor = assignmentGroup[0].course_color
        let courseId = assignmentGroup[0].course_id
        return[
          <ion-card style={{marginTop: "0"}}>
            <ion-item
              lines={'none'}
              style={{borderBottom:`1px solid ${this.assignmentGroupCourseColor}`}}
              onClick={() => this.courseClickedToView(courseId)}
            >
              {this.assignmentGroupCourseName}
            </ion-item>
            {this.renderAssignmentsForAllCourses(assignmentGroup)}
          </ion-card>
        ]
      })
    ]
  }

  render() {
    return [
      <ion-item lines={'none'} color={"light"}>
        {this.formatAssignmentGroupDueDate()}
      </ion-item>,
      this.listCourseNameForAssignments()
    ];
  }
}
