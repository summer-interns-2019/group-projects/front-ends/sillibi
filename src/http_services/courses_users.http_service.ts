import {CrudHttpService} from './crud.http_service';

export class CoursesUsersHttpService extends CrudHttpService {
  constructor() {
    super('courses_users');
  }
}
