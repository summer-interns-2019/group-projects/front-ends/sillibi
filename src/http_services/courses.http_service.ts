import {CrudHttpService} from './crud.http_service';

export class CoursesHttpService extends CrudHttpService {
  constructor() {
    super('courses');
  }
}
