import {CrudHttpService} from './crud.http_service';

export class AssignmentsHttpService extends CrudHttpService {
  constructor() {
    super('assignments');
  }
}
