import {CrudHttpService} from './crud.http_service';

export class InstitutionHttpService extends CrudHttpService {
  constructor() {
    super('institutions');
  }
}
