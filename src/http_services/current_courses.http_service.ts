import {CrudHttpService} from './crud.http_service';

export class CurrentCoursesHttpService extends CrudHttpService {
  constructor() {
    super('courses/current');
  }
}
