import {CrudHttpService} from './crud.http_service';

export class SyllabusesHttpService extends CrudHttpService {
  constructor() {
    super('syllabuses');
  }
}
