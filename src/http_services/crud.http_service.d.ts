export declare class CrudHttpService {
    url: string;
    constructor(apiUrl: string);
    where(params: any): Promise<any>;
    find(id: string): Promise<any>;
    update(payload: any): Promise<any>;
    delete(id: number): Promise<Response>;
    create(payload: any): Promise<any>;
    getRequest(fullUrl: any): Promise<any>;
}
