import {CrudHttpService} from './crud.http_service';

export class CourseHttpService extends CrudHttpService {
  constructor() {
    super('courses');
  }
}
