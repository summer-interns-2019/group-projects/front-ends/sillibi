import {CrudHttpService} from './crud.http_service';

export class ProfileHttpService extends CrudHttpService {
  constructor() {
    super('profiles');
  }
}
